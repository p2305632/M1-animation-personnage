/****************************************************************************
Copyright (C) 2010-2020 Alexandre Meyer

This file is part of Simea.

Simea is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Simea is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Simea.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef PARTICLES_H
#define PARTICLES_H

#include <iostream>
#include <vector>

#include "vec.h"

class Particle
{
public:

	Particle()
	{
		m_v = { 0.f, 0.f, 0.f };

		m_radius = 10 + rand() % 5;
		float volume = (4.f / 3.f) * M_PI * m_radius * m_radius * m_radius;
		m_mass = volume * 0.001f;      // 1g per m^3

		m_p.x = rand() % 400 - 200;
		m_p.y = m_radius + 5 + rand() % 100;
		m_p.z = rand() % 400 - 200;

		m_restitutionCoeff = .8f;
		m_characterStrength = 50;
		m_friction = 0.1f;
	}

	void update(const float dt = 0.1f)		// advect
	{
		if (m_mass > 0.f)
		{
			// Ajout de la fricton, colin�aire � la vitesse et de sens oppos�
			addForce(-m_v * m_friction);

			Vector a = m_f / m_mass;

			// mise � jour de la vitesse
			m_v = m_v + a * dt;

			// mise � jour de la position
			m_p = m_p + m_v * dt;

			// remise � 0 de la force
			m_f = { 0.f, 0.f, 0.f };
		}
	}

	//! Collision with the ground (y=0)
	void groundCollision()
	{
		if (m_radius < 0.f) return;

		if (m_p.y < m_radius)
		{
			m_p.y = m_radius;
			m_v.y *= -m_restitutionCoeff;
		}
	}

	//! Collision with any point p of radius radius (this will be used for kicking with the character's bones)
	void collisionWithPlayer(const Point& p, const float radius)
	{
		if (m_radius < 0.f) return;

		if (distance2(m_p, p) > (radius + m_radius) * (radius + m_radius)) return;

		Vector centerToParticleVec = normalize(Vector(m_p) - Vector(p));
		m_p = p + centerToParticleVec * (radius + m_radius);

		addForce(centerToParticleVec * m_characterStrength);
	}

	//! add force to the particles
	void addForce(const Vector& force)
	{
		m_f = m_f + force;
	}


	//! Apply gravity
	void addEarthGravity()
	{
		// apply gravity, call addForce
		addForce( Vector(0.f, -m_mass * 9.81f, 0.f) );
	}

	inline const Point& position() const { return m_p; }
	inline void setPosition(const Point& p) { m_p = p; }
	inline float radius() const { return m_radius; }
	inline float mass() const { return m_mass; }
	inline const Vector& velocity() const { return m_v; }
	inline void setVelocity(const Vector& v) { m_v = v; }

	friend std::ostream& operator<<(std::ostream& o, const Particle& p)
	{
		o << " p=(" << p.m_p.x << "," << p.m_p.y << ") v=(" << p.m_v.x << "," << p.m_p.y << "," << p.m_p.z << ") m=" << p.m_mass << std::endl;
		return o;
	}

protected:
	Point m_p;				//!< position
	float m_radius;			//!< radius
	Vector m_v;				//!< velocity m/s
	Vector m_f;				//!< force in N
	float m_mass;			//!< mass in kg
	float m_restitutionCoeff;
	float m_characterStrength; // in newtons
	float m_friction;
};


#endif
