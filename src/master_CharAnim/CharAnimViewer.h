
#ifndef _CHARANIMVIEWER_H
#define _CHARANIMVIEWER_H


#include "quaternion.h"
#include "Viewer.h"
#include "BVH.h"
#include "Skeleton.h"
#include "TransformQ.h"
#include "CharacterController.h"

#include <PhysicalWorld.h>

class CharAnimViewer : public Viewer
{
public:
    CharAnimViewer();

    int init();
    int render();
    int update( const float time, const float delta );

	static CharAnimViewer& singleton() { return *psingleton;  }

protected:
	void bvhDrawRec(const chara::BVHJoint& bvh, const Transform& f2w, int f);

    void updateCollisions();

    Skeleton m_ske;
    CharacterController m_CharController;

    PhysicalWorld m_world;

	void draw_skeleton(const Skeleton& );
    void draw_character_as_sphere(const CharacterController& character);


private:
	static CharAnimViewer* psingleton;
};

#endif
