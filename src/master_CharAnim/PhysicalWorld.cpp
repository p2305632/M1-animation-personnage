
#include <PhysicalWorld.h>

#include <CharAnimViewer.h>


void PhysicalWorld::update(const float dt)
{
	long unsigned int i;
	for (i = 0; i<m_part.size(); ++i)
	{
		// i_eme particule update
		m_part[i].update(dt);
		// i_eme particule collision
		m_part[i].groundCollision();
		// i_eme particule add gravirty
		m_part[i].addEarthGravity();
	}

	// Handle collision between particles

	collisionsBetweenParticles();
}


void PhysicalWorld::draw()
{
	long unsigned int i;
	for (i = 0; i<particlesCount(); ++i)
	{
		if (m_part[i].radius()>0)
			CharAnimViewer::singleton().draw_sphere(m_part[i].position(), m_part[i].radius());
	}
}


void PhysicalWorld::collisionWithPlayer(const Point& p, const float radius)
{
	long unsigned int i;
	for (i = 0; i<m_part.size(); ++i)
	{
		m_part[i].collisionWithPlayer(p,radius);
	}
}

void PhysicalWorld::collisionsBetweenParticles()
{
	for (size_t i = 0; i < m_part.size() - 1; i++)
	{
		for (size_t j = i + 1; j < m_part.size(); j++)
		{
			resolveColls(m_part[i], m_part[j]);
		}
	}
}

void PhysicalWorld::resolveColls(Particle& A, Particle& B)
{
	if (distance2(A.position(), B.position()) > (A.radius() + B.radius()) * (A.radius() + B.radius()))
		return; // No coll

	float overlapping = A.radius() + B.radius() - distance(A.position(), B.position());

	Vector nAB = normalize(B.position() - A.position());

	A.setPosition(A.position() - nAB * overlapping / 2.f);
	B.setPosition(B.position() + nAB * overlapping / 2.f);

	const Vector& vAi = A.velocity(), &vBi = B.velocity();

	Vector vAf = ((A.mass() - B.mass()) * dot(vAi, nAB) * nAB + 2.f * B.mass() * dot(vBi, nAB) * nAB) / (A.mass() + B.mass()) + vAi - dot(vAi, nAB) * nAB;
	Vector vBf = ((B.mass() - A.mass()) * dot(vBi, nAB) * nAB + 2.f * A.mass() * dot(vAi, nAB) * nAB) / (A.mass() + B.mass()) + vBi - dot(vBi, nAB) * nAB;

	A.setVelocity(vAf);
	B.setVelocity(vBf);
}
