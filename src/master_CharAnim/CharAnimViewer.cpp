
#include <cassert>
#include <cmath>
#include <cstdio>
#include <iostream>

#include "CharAnimViewer.h"

using namespace std;
using namespace chara;

const float JOINT_RADIUS = 2.f;

CharAnimViewer* CharAnimViewer::psingleton = NULL;


CharAnimViewer::CharAnimViewer() : Viewer()
{
	psingleton = this;
}


int CharAnimViewer::init()
{
    Viewer::init();
    cout<<"==>master_CharAnim/CharAnimViewer"<<endl;
    m_camera.lookat( Point(0,0,0), 1000 );
	m_camera.rotation(180, 0);
    gl.light( Point(300, 300, 300 ) );

    //b_draw_grid = false;

    m_world.setParticlesCount( 10 );


    init_cylinder();
    init_sphere();


    //m_bvh.init( smart_path("data/bvh/Robot.bvh") );
    chara::BVH bvh;
    bvh.init(smart_path("data/bvh/motionGraph_second_life/avatar_smoke_idle.bvh"));

    m_ske.init( bvh );

    return 0;
}



void CharAnimViewer::draw_skeleton(const Skeleton& ske)
{
	for (int i = 0; i < ske.numberOfJoint(); i++)
	{
		auto pos = m_CharController.getLocalToWorld()(ske.getJointPosition(i));
        
		draw_sphere(pos, JOINT_RADIUS);

		if (ske.getParentId(i) < 0) continue;

		auto parentPos = m_CharController.getLocalToWorld()(ske.getJointPosition(ske.getParentId(i)));

		draw_cylinder(pos, parentPos);
	}
}

void CharAnimViewer::draw_character_as_sphere(const CharacterController& character)
{
    draw_sphere(character.position(), 10.f);
}

int CharAnimViewer::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //draw_quad( RotationX(-90)*Scale(500,500,1) );

    Viewer::manageCameraLight();
    gl.camera(m_camera);

	// Affiche les particules physiques (Question 3 : interaction personnage sphere/ballon)
    m_world.draw();


	// Affiche le skeleton � partir de la structure lin�aire (tableau) Skeleton
    draw_skeleton( m_ske );

    // Affiche le personnage (sphere)
    //draw_character_as_sphere( m_CharController );

    return 1;
}


int CharAnimViewer::update( const float time, const float delta )
{
    // time est le temps ecoule depuis le demarrage de l'application, en millisecondes,
    // delta est le temps ecoule depuis l'affichage de la derniere image / le dernier appel a draw(), en millisecondes.

    float deltaSeconds = delta / 1000.f;

    // deg per second
    static float turnRate = 180.f;

    // u per second per second where u is distance unit.
    static float acceleration = 50.f;

    if (key_state('q'))
        m_CharController.turnXZ(turnRate * deltaSeconds);
    
    if (key_state('d'))
        m_CharController.turnXZ(-turnRate * deltaSeconds);

    if (key_state('z'))
        m_CharController.accelerate(acceleration * deltaSeconds);

    if (key_state('s'))
        m_CharController.accelerate(-acceleration * deltaSeconds);

    // Just to force user to press twice his x button if he wants to kick twice (we don't want to repeateadly kick each frame when x is held down).
    static bool old_kick = false;

    if (key_state('x'))
    {
        if (old_kick == false)
        {
            m_CharController.kick();
        }

        old_kick = true;
    }
    else
        old_kick = false;

    static bool old_jump = false;

    if (key_state(' '))
    {
        if (old_jump == false)
        {
            m_CharController.jump();
        }

        old_jump = true;
    }
    else
        old_jump = false;

    m_CharController.update(deltaSeconds);
    m_CharController.getAnimationStateMachine().updateSkeleton(m_ske);
    m_world.update(0.1f);
    updateCollisions();

    return 0;
}

void CharAnimViewer::updateCollisions()
{
    for (int i = 0; i < m_ske.numberOfJoint(); i++)
    {
        auto pos = m_CharController.getLocalToWorld()(m_ske.getJointPosition(i));

        m_world.collisionWithPlayer(pos, JOINT_RADIUS);
    }
}



