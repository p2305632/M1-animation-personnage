#include "CharacterController.h"

const float LOCOMOTION_BLEND_TIME = 0.3f;
const float KICKING_BLEND_TIME = 0.2f;
const float JUMPING_BLEND_TIME = 0.2f;

CharacterController::CharacterController()
	: m_Forward(1.f, 0.f, 0.f),
	m_Velocity(0.f), m_VelocityMax(100.f)
{
	m_ASM.addAnimation("data/bvh/motionGraph_second_life/avatar_smoke_idle.bvh", "idle");
	m_ASM.addAnimation("data/bvh/motionGraph_second_life/avatar_walk.bvh", "walk");
	m_ASM.addAnimation("data/bvh/motionGraph_second_life/avatar_run.bvh", "run");

	m_ASM.addAnimation("data/bvh/motionGraph_second_life/avatar_kick_roundhouse_R.bvh", "kick");

	m_ASM.addAnimation("data/bvh/motionGraph_second_life/avatar_prejump.bvh", "prejump");
	m_ASM.addAnimation("data/bvh/motionGraph_second_life/avatar_jump.bvh", "jump");
	m_ASM.addAnimation("data/bvh/motionGraph_second_life/avatar_land.bvh", "land");

	// Locomotion
	m_ASM.addLink("idle", "walk", [this]() { return m_Velocity > 0.1f; }, LOCOMOTION_BLEND_TIME);
	m_ASM.addLink("walk", "idle", [this]() { return m_Velocity < 0.1f; }, LOCOMOTION_BLEND_TIME);

	m_ASM.addLink("walk", "run", [this]() { return m_Velocity > m_VelocityMax / 2.f; }, LOCOMOTION_BLEND_TIME);
	m_ASM.addLink("run", "walk", [this]() { return m_Velocity < m_VelocityMax / 2.f; }, LOCOMOTION_BLEND_TIME);

	// Kicking
	m_ASM.addLink("idle", "kick", [this]() { return m_Kicking; }, KICKING_BLEND_TIME);
	m_ASM.addLink("walk", "kick", [this]() { return m_Kicking; }, KICKING_BLEND_TIME);
	m_ASM.addLink("run", "kick", [this]() { return m_Kicking; }, KICKING_BLEND_TIME);

	m_ASM.addLink("kick", "idle", [this]() { return !m_Kicking; }, KICKING_BLEND_TIME);

	// Jumping
	m_ASM.addLink("idle", "prejump", [this]() { return m_Jumping; }, 0.1f);
	m_ASM.addLink("walk", "prejump", [this]() { return m_Jumping; }, 0.1f);
	m_ASM.addLink("run", "prejump", [this]() { return m_Jumping; }, 0.1f);

	m_ASM.addLink("prejump", "jump", [this]() { return m_JumpTimer > m_PrejumpDuration; }, .1f);
	m_ASM.addLink("jump", "land", [this]() { return m_JumpTimer > m_PrejumpDuration + m_JumpDuration; }, .1f);

	m_ASM.addLink("land", "idle", [this]() { return !m_Jumping; }, .3f);

	m_ASM.setStartingAnimation("idle");
	m_ASM.start();

	m_KickDuration = m_ASM.getBVH("kick").getTotalTime();
	m_PrejumpDuration = m_ASM.getBVH("prejump").getTotalTime();
	m_JumpDuration = m_ASM.getBVH("jump").getTotalTime();
	m_LandDuration = m_ASM.getBVH("land").getTotalTime();
}

void CharacterController::update(float dt)
{
	//std::cout << "Updating character controller for " << dt << " seconds.\n";
	//std::cout << "Velocity: " << m_Velocity << " / " << m_VelocityMax << '\n';
	//std::cout << "Friction: " << m_Friction << "\n";
	//std::cout << "Direction: " << m_Forward << '\n';

	// Updating position
	Vector delta = dt * m_Velocity * m_Forward;
	m_Pos = m_Pos + delta;
	m_LocalToWorldChanged = true;

	// Handling kicking
	if (m_Kicking)
	{
		if (m_KickTimer > m_KickDuration)
			m_Kicking = false;

		m_KickTimer += dt;
	}

	// Handle jumping
	if (m_Jumping)
	{
		m_JumpTimer += dt;
		
		if (m_JumpTimer > m_PrejumpDuration + m_JumpDuration + m_LandDuration)
			m_Jumping = false;
	}

	// Update animations
	m_ASM.update(dt);
}

void CharacterController::turnXZ(float rot_angle_v)
{
	m_Forward = RotationY(rot_angle_v)(m_Forward);
}

void CharacterController::accelerate(float speed_inc)
{
	m_Velocity += speed_inc;

	// Clamping velocity in [0, m_VelocityMax]
	if (m_Velocity > m_VelocityMax) m_Velocity = m_VelocityMax;
	else if (m_Velocity < 0.f) m_Velocity = 0.f;
}

void CharacterController::setVelocityMax(float vmax)
{
	m_VelocityMax = vmax;
}

void CharacterController::kick()
{
	m_Kicking = true;
	m_KickTimer = 0.f;
}

void CharacterController::jump()
{
	m_Jumping = true;
	m_JumpTimer = 0.f;
}

const Point CharacterController::position() const
{
	return m_Pos;
}

const Vector CharacterController::direction() const
{
	return m_Forward;
}

Transform CharacterController::getLocalToWorld() const
{
	if (m_LocalToWorldChanged)
	{
		computeLocalToWorld();
		m_LocalToWorldChanged = false;
	}

	return m_CachedLocalToWorld;
}

void CharacterController::computeLocalToWorld() const
{
	static const Vector skeletonDefaultDir = { 0.f, 0.f, 1.f };

	m_CachedLocalToWorld = Translation(Vector(position())) * Rotation(skeletonDefaultDir, direction());
}
