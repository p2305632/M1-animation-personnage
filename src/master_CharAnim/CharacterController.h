#pragma once

#include "vec.h"
#include "mat.h"

#include "AnimationStateMachine.h"

class CharacterController
{
public:
    CharacterController();

    /**
     * @brief Updates controller.
     * @param dt Time in seconds.
     */
    void update(float dt);

    void turnXZ(float rot_angle_v);
    void accelerate(float speed_inc);
    void setVelocityMax(float vmax);
    void kick();
    void jump();

    const Point position() const;
    const Vector direction() const;
    Transform getLocalToWorld() const;
    float velocity() const { return m_Velocity; }

    inline const AnimationStateMachine& getAnimationStateMachine() const { return m_ASM; }

private:
    void computeLocalToWorld() const;

protected:
    AnimationStateMachine m_ASM;

    Point m_Pos;
    Vector m_Forward;

    float m_Velocity;
    float m_VelocityMax;

    bool m_Kicking = false;
    float m_KickTimer = 0.f, m_KickDuration = 2.f;

    bool m_Jumping = false;
    float m_JumpTimer = 0.f, m_PrejumpDuration = 0.f, m_JumpDuration = 0.f, m_LandDuration = 0.f;

    mutable Transform m_CachedLocalToWorld;
    mutable bool m_LocalToWorldChanged = true;
};