
#include "Skeleton.h"

using namespace chara;

void Skeleton::init(const BVH& bvh)
{
    //TODO
    int n = bvh.getNumberOfJoint();
    m_joints.reserve(n);

    for (int i = 0; i < n; i++)
    {
        const auto& bvhJoint = bvh.getJoint(i);
        float x, y, z;
        bvhJoint.getOffset(x, y, z);
        Transform t = Translation(x, y, z);

        auto skelJoint = SkeletonJoint{
            bvhJoint.getParentId(),
            std::move(t)
        };

        m_joints.push_back(std::move(skelJoint));
    }
}


Point Skeleton::getJointPosition(int i) const
{
    assert(i >= 0 && i < m_joints.size());
    return m_joints[i].m_l2w(Point(0.f, 0.f, 0.f));
}


int Skeleton::getParentId(const int i) const
{
    assert(i >= 0 && i < m_joints.size());
    return m_joints[i].m_parentId;
}


void Skeleton::setPose(const BVH& bvh, int frameNumber)
{
    for (int i = 0; i < bvh.getNumberOfJoint(); i++)
    {
        const auto& bvhJoint = bvh.getJoint(i);
        float x, y, z;
        bvhJoint.getOffset(x, y, z);
        Transform l2f = Translation(x, y, z);

        for (int c = 0; c < bvhJoint.getNumberOfChannel(); c++)
        {
            const BVHChannel& channel = bvhJoint.getChannel(c);
            Transform channelTransform;

            if (channel.isTranslation())
            {
                auto axis = channel.getAxis();

                switch (axis)
                {
                case AXIS_X:
                    channelTransform = Translation(channel.getData(frameNumber), 0.f, 0.f);
                    break;
                case AXIS_Y:
                    channelTransform = Translation(0.f, channel.getData(frameNumber), 0.f);
                    break;
                case AXIS_Z:
                    channelTransform = Translation(0.f, 0.f, channel.getData(frameNumber));
                    break;
                default:
                    throw std::runtime_error("Only translate along X, Y or Z axises.");
                    break;
                }
            }

            if (channel.isRotation())
            {
                auto axis = channel.getAxis();
                float angle = channel.getData(frameNumber);

                switch (axis)
                {
                case AXIS_X:
                    channelTransform = RotationX(angle);
                    break;
                case AXIS_Y:
                    channelTransform = RotationY(angle);
                    break;
                case AXIS_Z:
                    channelTransform = RotationZ(angle);
                    break;
                default:
                    throw std::runtime_error("Only rotate along X, Y or Z axises.");
                    break;
                }

            }

            l2f = l2f * channelTransform;
        }

        if (bvhJoint.isRoot())
        {
            // Pas de p�re. Le p�re est donc en quelque sorte "world".
            m_joints[i].m_l2w = l2f;
            continue; // Joint suivant.
        }
        else
        {
            int parentId = getParentId(i);
            m_joints[i].m_l2w = m_joints[parentId].m_l2w * l2f;
        }

    }
}

void Skeleton::setPoseInterpolation(const BVH& bvhSrc, int frameNbSrc, const BVH& bvhDst, int frameNbDst, float t)
{
    for (int i = 0; i < bvhSrc.getNumberOfJoint(); i++)
    {
        const auto& bvhJointSrc = bvhSrc.getJoint(i);
        const auto& bvhJointDst = bvhDst.getJoint(i);
        float x, y, z;
        bvhJointSrc.getOffset(x, y, z);
        Transform l2f = Identity();

        for (int c = 0; c < bvhJointSrc.getNumberOfChannel(); c++)
        {
            const BVHChannel& channelSrc = bvhJointSrc.getChannel(c);
            const BVHChannel& channelDst = bvhJointDst.getChannel(c);
            Transform channelTransform;

            if (channelSrc.isTranslation())
            {
                auto axis = channelSrc.getAxis();

                float translationValueSrc = channelSrc.getData(frameNbSrc);
                float translationValueDst = channelDst.getData(frameNbDst);
                float translationValue = translationValueSrc + (translationValueDst - translationValueSrc) * t;

                switch (axis)
                {
                case AXIS_X:
                    channelTransform = Translation(translationValue, 0.f, 0.f);
                    break;
                case AXIS_Y:
                    channelTransform = Translation(0.f, translationValue, 0.f);
                    break;
                case AXIS_Z:
                    channelTransform = Translation(0.f, 0.f, translationValue);
                    break;
                default:
                    throw std::runtime_error("Only translate along X, Y or Z axises.");
                    break;
                }
            }

            if (channelSrc.isRotation())
            {
                auto axis = channelSrc.getAxis();
                float angleSrc = channelSrc.getData(frameNbSrc);
                float angleDst = channelDst.getData(frameNbDst);
                float angle = angleSrc + (angleDst - angleSrc) * t;

                switch (axis)
                {
                case AXIS_X:
                    channelTransform = RotationX(angle);
                    break;
                case AXIS_Y:
                    channelTransform = RotationY(angle);
                    break;
                case AXIS_Z:
                    channelTransform = RotationZ(angle);
                    break;
                default:
                    throw std::runtime_error("Only rotate along X, Y or Z axises.");
                    break;
                }

            }

            l2f = l2f * channelTransform;
        }

        l2f = Translation(x, y, z) * l2f;

        if (bvhJointSrc.isRoot())
        {
            // Pas de p�re. Le p�re est donc en quelque sorte "world".
            m_joints[i].m_l2w = l2f;
            continue; // Joint suivant.
        }
        else
        {
            int parentId = getParentId(i);
            m_joints[i].m_l2w = m_joints[parentId].m_l2w * l2f;
        }

    }
}