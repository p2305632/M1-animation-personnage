#include "AnimationStateMachine.h"

#include "window.h"
#include "Skeleton.h"

// Comment to disable debugging
#define DEBUG_ASM

#ifdef DEBUG_ASM
#include <chrono>
#endif

AnimationStateMachine::AnimationStateMachine()
	: m_CurrentAnim(m_Animations.cend()), m_LastAnimation(m_Animations.cend()), m_StartingAnimation(m_Animations.cend())
{

}

void AnimationStateMachine::update(float dt)
{
	// TIME DILATION FOR DEBUG
	// dt = dt * 0.1f;

	if (m_CurrentAnim == m_Animations.cend())
	{
		throw std::runtime_error("Cannot update an AnimationStateMachine with no current animation. State machine has probably not been started.");
	}

	/****** Handle transitions *****/

	const std::string& currentAnimName = m_CurrentAnim->first;

	// If there is no link starting with current anim, we can just return (state machine is blocked forever).
	if (m_Links.find(currentAnimName) != m_Links.end())
	{
		const auto& possibleLinks = m_Links.at(currentAnimName);

		// Check if a link has a valid transition
		auto transitionLink = std::find_if(
			possibleLinks.cbegin(),
			possibleLinks.cend(),
			[](const std::pair<std::string, Link>& pair) -> bool {
				return pair.second.canGo();
			}
		);

		if (transitionLink != possibleLinks.end())
			playAnimationWithBlend(transitionLink->first, transitionLink->second.blendTime);
	}


	/****** Handle blending ******/

	m_EllapsedTimeSinceLastAnim += dt;
}

void AnimationStateMachine::addAnimation(const std::string& path)
{
	// We add the animation with its path as an identifier.
	addAnimation(path, path);
}

void AnimationStateMachine::addAnimation(const std::string& path, const std::string& name)
{
	chara::BVH bvh;
	bvh.init(smart_path(path.c_str()));

	addAnimation(std::move(bvh), name);
}

void AnimationStateMachine::addAnimation(chara::BVH&& bvh, const std::string& name)
{
	if (m_Animations.find(name) != m_Animations.end())
	{
		throw std::runtime_error("Can't add two animations with the same identifier/name.");
	}

	m_Animations.insert({ name, std::move(bvh) });
}

void AnimationStateMachine::addLink(const std::string& fromAnim, const std::string& toAnim, const TransitionFunction& transition, float blendTime)
{
	if (m_Links[fromAnim].find(toAnim) != m_Links[fromAnim].end())
	{
		throw std::runtime_error("Can't add two links with same from and to animations.");
	}

	m_Links[fromAnim][toAnim] = Link{ fromAnim, toAnim, transition, blendTime };
}

void AnimationStateMachine::setStartingAnimation(const std::string& anim)
{
	AnimationsContainer::const_iterator animIt = m_Animations.find(anim);

	if (animIt == m_Animations.end())
	{
		throw std::runtime_error("Couldn't find animation \"" + anim + "\" in AnimationStateMachine.");
	}

	m_StartingAnimation = animIt;
}

void AnimationStateMachine::start()
{
	if (m_StartingAnimation == m_Animations.end())
	{
		throw std::runtime_error("Error while starting AnimationStateMachine: no starting animation was set up.");
	}

	playAnimation(m_StartingAnimation);
	m_LastAnimation = m_StartingAnimation; // So we don't start with iterator::end() as previous anim.
}

void AnimationStateMachine::updateSkeleton(Skeleton& skeleton) const
{
#ifdef DEBUG_ASM
	static auto lastUpdateTime = std::chrono::high_resolution_clock::now();
	static bool shouldDisplay_debug = false;

	auto currentTime = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsedTime = currentTime - lastUpdateTime;

	if (elapsedTime.count() >= 0.1)
	{
		shouldDisplay_debug = true;
		lastUpdateTime = currentTime;
	}
	else
		shouldDisplay_debug = false;

	if (shouldDisplay_debug)
		std::cout << "[DEBUG] AnimationStateMachine";
#endif

	const chara::BVH& currentBVH = getCurrentAnimation();

	// We need to know what frame we are at in the current animation
	int currentFrameNumber = static_cast<int>(m_EllapsedTimeSinceLastAnim / currentBVH.getFrameTime());
	// For now, currentFrameNumber stores the number of frames that have been played since 
	currentFrameNumber = currentFrameNumber % currentBVH.getNumberOfFrame();
	// Now it really stores the last played frame (we may have looped multiple times in current animation).

	// If we currently are in a transition between two animations (previous and current) and the blending is not finished
	if (isBlending())
	{
		const chara::BVH& previousBVH = getPreviousAnimation();

		// We need to know at wich frame we stopped last animation
		int previousFrameNumber = static_cast<int>(m_OldEllapsedTimeSinceLastAnim / previousBVH.getFrameTime());
		previousFrameNumber = previousFrameNumber % previousBVH.getNumberOfFrame(); // Because we may have looped multiple times in previous animation.

		// We need to know the blending of both frames
		float blending = m_EllapsedTimeSinceLastAnim / m_CurrentBlendTime;

		//{ // @TODO Remove this. Just for testing.
		//	skeleton.setPose(currentBVH, currentFrameNumber);
		//}

		// Now we can blend
		skeleton.setPoseInterpolation(previousBVH, previousFrameNumber, currentBVH, currentFrameNumber, blending);

#ifdef DEBUG_ASM
		if (shouldDisplay_debug)
		{
			std::cout << " BLENDING.\n"
				<< "From animation \"" << m_LastAnimation->first << "\" to animation \"" << m_CurrentAnim->first << "\".\n"
				<< "From frame " << previousFrameNumber << " to frame " << currentFrameNumber << " with blend=" << blending << ".\n";
		}
#endif

	}
	// Else, we are between two frames of the current animation
	else
	{
		int nextFrameNumber = (currentFrameNumber + 1) % currentBVH.getNumberOfFrame();
		float timeSinceLastFrame = std::fmodf(m_EllapsedTimeSinceLastAnim, currentBVH.getFrameTime());
		float blending = timeSinceLastFrame / currentBVH.getFrameTime();

		skeleton.setPoseInterpolation(currentBVH, currentFrameNumber, currentBVH, nextFrameNumber, blending);

#ifdef DEBUG_ASM
		if (shouldDisplay_debug)
		{
			std::cout << "\n"
				<< "Playing animation \"" << m_CurrentAnim->first << "\".\n"
				<< "From frame " << currentFrameNumber << " to frame " << nextFrameNumber << " with blend=" << blending << ".\n";
		}
#endif

	}

#ifdef DEBUG_ASM
	std::cout << std::flush;
#endif

}

bool AnimationStateMachine::isBlending() const
{
	// m_CurrentBlendTime CAN be EXACTLY zero, because we set it on purpose to zero when we don't want any blending.
	return m_CurrentBlendTime > 0.f && m_EllapsedTimeSinceLastAnim < m_CurrentBlendTime;
}

void AnimationStateMachine::playAnimation(const std::string& animation)
{
	AnimationsContainer::const_iterator animToGoIt = m_Animations.find(animation);

	if (animToGoIt == m_Animations.cend())
	{
		throw std::runtime_error("Couldn't find animation \"" + animation + "\" in AnimationStateMachine.");
	}

	playAnimation(animToGoIt);
}

void AnimationStateMachine::playAnimation(const AnimationsContainer::const_iterator& animation)
{
	m_LastAnimation = m_CurrentAnim;
	m_CurrentAnim = animation;

	m_CurrentBlendTime = 0.f; // No blend time in this function.
	m_OldEllapsedTimeSinceLastAnim = m_EllapsedTimeSinceLastAnim;
	m_EllapsedTimeSinceLastAnim = 0.f;

	std::cout << "Playing animation \"" + m_CurrentAnim->first + "\". ";
	std::cout << " Old animation : \"" + (m_LastAnimation != m_Animations.cend() ? m_LastAnimation->first : "null") + "\".\n";
}

void AnimationStateMachine::playAnimationWithBlend(const std::string& animation, float blendTime)
{
	AnimationsContainer::const_iterator animToGoIt = m_Animations.find(animation);

	if (animToGoIt == m_Animations.cend())
	{
		throw std::runtime_error("Couldn't find animation \"" + animation + "\" in AnimationStateMachine.");
	}

	playAnimationWithBlend(animToGoIt, blendTime);
}

void AnimationStateMachine::playAnimationWithBlend(const AnimationsContainer::const_iterator& animation, float blendTime)
{
	m_LastAnimation = m_CurrentAnim;
	m_CurrentAnim = animation;

	m_CurrentBlendTime = blendTime;
	m_OldEllapsedTimeSinceLastAnim = m_EllapsedTimeSinceLastAnim;
	m_EllapsedTimeSinceLastAnim = 0.f;

	std::cout << "Playing animation \"" + m_CurrentAnim->first + "\". ";
	std::cout << " Old animation : \"" + (m_LastAnimation != m_Animations.cend() ? m_LastAnimation->first : "null") + "\".\n";
}