#pragma once

#include <unordered_map>
#include <string>
#include <functional>

#include "BVH.h"

class Skeleton;

/**
 * @brief AnimationStateMachine is a class that stores animations and manages transitions between them.
 */
class AnimationStateMachine
{
public: // Public aliases

	/**
	 * @brief Returns true if the transition can be made, false otherwise.
	 */
	using TransitionFunction = std::function<bool()>;

public: // Public member structures
	
	/**
	 * @brief Default transition functor always returns false. This means the transition will never be made.
	 */
	struct DefaultTransitionFunction
	{
		bool operator() () const { return false; }
	};


public: // Public methods

	AnimationStateMachine();
	virtual ~AnimationStateMachine() = default;

	/**
	 * @brief Needs to be called each frame. Will check if a transition can be made.
	 * @param controller The CharacterController in use.
	 * @param dt Time since last update.
	 */
	void update(float dt);

	/**
	 * @brief Adds an animation to the machine.
	 * @param path File path of the animation bvh. Then the animation can be retrieved by its path.
	 */
	void addAnimation(const std::string& path);

	/**
	 * @brief Adds an animation to the machine.
	 * @param path File path of the animation bvh.
	 * @param name The unique name used to retrieve the animation.
	 */
	void addAnimation(const std::string& path, const std::string& name);

	/**
	 * @brief Adds an animation to the machine.
	 * @param bvh An r-value reference to the bvh. Will be moved to the machine.
	 * @param name The unique name used to retrieve the animation.
	 */
	void addAnimation(chara::BVH&& bvh, const std::string& name);

	/**
	 * @brief Adds a link between two animations.
	 * @param fromAnim Animation the transition applies from.
	 * @param toAnim Animation the transition applies to.
	 * @param transition Transition function.
	 * @param blendTime Transition function.
	 */
	void addLink(const std::string& fromAnim, const std::string& toAnim, const TransitionFunction& transition = DefaultTransitionFunction(), float blendTime = 0.f);

	/**
	 * @brief Sets the starting animation.
	 * @param anim Identifier of the animation;
	 */
	void setStartingAnimation(const std::string& anim);

	/**
	 * @brief Starts the machine.
	 */
	void start();

	/**
	 * @brief Gets the current animation BVH.
	 */
	const chara::BVH& getCurrentAnimation() const { return m_CurrentAnim->second; }

	/**
	 * @brief Gets the last previous BVH.
	 */
	const chara::BVH& getPreviousAnimation() const { return m_LastAnimation->second; }

	/**
	 * @brief Sets pose of the skeleton depending of the state of the machine.
	 * @param skeleton The skeleton the pose needs to be updated.
	 */
	void updateSkeleton(Skeleton& skeleton) const;

	/**
	 * @brief Gets a BVH by its identifier.
	 * @param identifier Animation name.
	 * @throws out_of_bounds If identifer does not exits.
	 */
	const chara::BVH& getBVH(const std::string& identifier) { return m_Animations.at(identifier); }

private: // Private aliases

	using AnimationsContainer = std::unordered_map<std::string, chara::BVH>;

private: // Private member structures

	struct Link
	{
		std::string fromAnim, toAnim;
		TransitionFunction canGo;
		float blendTime = 0.f;
	};

private: // Private methods

	/**
	 * @brief Plays an animation without blend.
	 * @param animation The animation to play.
	 */
	void playAnimation(const std::string& animation);

	/**
	 * @brief Plays animation without blend.
	 * @param animation The animation to play.
	 */
	void playAnimation(const AnimationsContainer::const_iterator& animation);

	/**
	 * @brief Plays an animation with blend time.
	 * @param animation The animation to play.
	 * @param blendTime Blending time.
	 */
	void playAnimationWithBlend(const std::string& animation, float blendTime);

	/**
	 * @brief Plays an animation with blend time.
	 * @param animation The animation to play.
	 */
	void playAnimationWithBlend(const AnimationsContainer::const_iterator& animation, float blendTime);

	/**
	 * @brief To know if the state machine is currently blending between two states (transition between two different anims).
	 * @return True if blending, false otherwise.
	 */
	bool isBlending() const;

private: // Private variables
	AnimationsContainer m_Animations;

	std::unordered_map<std::string, std::unordered_map<std::string, Link>> m_Links;

	AnimationsContainer::const_iterator m_CurrentAnim, m_LastAnimation, m_StartingAnimation;

	float m_CurrentBlendTime = 0.f; // For how long do we need to blend two animations ?
	float m_EllapsedTimeSinceLastAnim = 0.f; // So we can keep track of blending.
	float m_OldEllapsedTimeSinceLastAnim = 0.f; // So we can remember where we stopped playing previous animation.
};